package lk.dialog.gtpd.training.tdd.tests;

import lk.dialog.gtpd.training.tdd.operations.SmsCreator;
import org.junit.Assert;
import org.junit.Test;



/**
 * Author: sambestsolutions
 * Date : 7/15/2019
 * Time : 9:51 AM
 * Created by :  IntelliJ IDEA.
 * Project : demo-customer-sms-service
 */
public class SmsCreationTest {

    //private Logger logger = LogManager.getLogger(getClass());

    @Test
    public void testSmsContent(){
       String expectedString = "Thank you for visiting Dialog this morning";
        SmsCreator smsCreator = new SmsCreator();

        String result= smsCreator.getSmsContent();

        Assert.assertEquals(expectedString, result);
    }

    @Test
    public void testSmsContentMorning(){
        String expectedString = "Thank you for visiting Dialog this morning";
        SmsCreator smsCreator = new SmsCreator();

        String result= smsCreator.getSmsContent();

        Assert.assertEquals(expectedString, result);
    }
}
