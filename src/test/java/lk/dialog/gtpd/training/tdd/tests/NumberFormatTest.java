package lk.dialog.gtpd.training.tdd.tests;

import lk.dialog.gtpd.training.tdd.operations.NumberFormatter;
import org.junit.Assert;
import org.junit.Test;

/**
 * Author: sambestsolutions
 * Date : 7/15/2019
 * Time : 10:02 AM
 * Created by :  IntelliJ IDEA.
 * Project : demo-customer-sms-service
 */
public class NumberFormatTest {

    @Test
    public void numberFormatCheck(){
        String[] numbers = {"777331419","0773337900"};

        String[] expectedNumbers = {"94777331419","94773337900"};

        NumberFormatter numberFormatter = new NumberFormatter();


        for(int i=0 ; i< numbers.length ; i++)
        {
            String outcome = numberFormatter.getCompatibleNumberFormat(numbers[i]);

            Assert.assertEquals(expectedNumbers[i],outcome);
        }
    }



}
