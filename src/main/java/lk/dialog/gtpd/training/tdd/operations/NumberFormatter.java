package lk.dialog.gtpd.training.tdd.operations;

public class NumberFormatter {


    /**
     * Gets the compatible number for the SMS system from the customer input.
     *   Will return null if the number is not a Dialog number.
     * @param number number which the customer entered
     * @return properly formatted dialog number or null if user is not from Dialog
     */
    public String getCompatibleNumberFormat(String number) {
        return number.replaceAll("^(0|94)?(7[67]\\d+)","94$2");
        //return number;
    }
}
